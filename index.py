#!/usr/bin/python
import random
import os
import jinja2

env = jinja2.Environment(
    loader=jinja2.FileSystemLoader(os.path.dirname(__file__)),
    extensions=['jinja2.ext.autoescape'],
    autoescape=True
)

def read_list(file):
    for line in file:
        line = line.strip()
        if line[:1] == '#':
            continue
        if line == '':
            continue
        yield line

def load_list(name):
    with open(name) as f:
        return list(read_list(f))

def get_index(request):
    verbs = load_list('words/verb.txt')
    adverbs = load_list('words/adverb.txt')
    adjectives = load_list('words/adjective.txt')
    nouns = load_list('words/noun.txt')
    digits = [ str(i) for i in range(0, 9) ]

    adverb = random.choice(adverbs)
    verb = random.choice(verbs)
    adjective = random.choice(adjectives)
    noun = random.choice(nouns)
    results = adverb + " " + verb + " " + adjective + " " + noun

    digit1 = random.choice(digits)
    digit2 = random.choice(digits)
    digit3 = random.choice(digits)
    resultn = adverb + digit1 + verb + digit2 + adjective + digit3 + noun

    return env.get_template('output.html').render(
        results=results,
        resultn=resultn
    )

if __name__ == '__main__':
    print "Content-type: text/html"
    print
    print get_index(None)
